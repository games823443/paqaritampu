# Paqaritampu

_Portfolio: https://portfolio.fh-salzburg.ac.at/projects/2019-paquaritampu-vr-mmp3-wip_

_Demo video: https://www.youtube.com/watch?v=A31w1lgmYTo_

## Description
Paqaritampu VR is a horror, puzzle game that was developed by students of the University of Applied Sciences Salzburg.
The game is developed for the Oculus Quest using Unity where you can play
on a field of 7x7 meters and walk freely by yourself.

## Gamestory Summary
„Paqaritampu VR“ is a storybased adventure/strategy/horror game.
The unknowingly chosen descendant of sungod Inti is looking
for clues of her past in a jungle together with her friends. Due to a
sudden storm, they end up in a hidden cave. The friends decide to
explore it, since there is nothing else to do because of the horrible
weather. In there, strange things start happening and soon the protagonist (Koya Evans) is not only looking for her friendsbut also a
way out...
