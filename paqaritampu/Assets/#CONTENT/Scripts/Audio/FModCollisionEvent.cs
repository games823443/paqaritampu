using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[AddComponentMenu("Fmod/FModCollisionEvent")]
public class FModCollisionEvent : MonoBehaviour
{
    // Define FMOD Event for Collision Sound
    [FMODUnity.EventRef]
    public string CollisionEvent;
    FMOD.Studio.EventInstance Collision;

    void OnCollisionEnter(Collision c)
    {
        // Play Audio when fractured
        Collision = FMODUnity.RuntimeManager.CreateInstance(CollisionEvent);
        Collision.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        Collision.start();
        //
    }

}
