﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEventOneShot : MonoBehaviour
{
    [FMODUnity.EventRef]
    [SerializeField] private string ObjectEvent;
    private FMOD.Studio.EventInstance _eventInstance;

    private bool _done = false;

    public void PlaySound()
    {
        if (!_done)
        {
            _eventInstance = FMODUnity.RuntimeManager.CreateInstance(ObjectEvent);
            _eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));

            _eventInstance.start();
            _done = true;
        }
    }
}
