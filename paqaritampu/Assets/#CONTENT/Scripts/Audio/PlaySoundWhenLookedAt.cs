﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundWhenLookedAt : MonoBehaviour
{
    [SerializeField] private Transform _centerEye;
    [SerializeField] private float _angle = 10.0f;

    [SerializeField] private AudioEvent[] _audioEvents;

    private bool _done = false;
    private bool _playerInSight = false;

    // Update is called once per frame
    void Update()
    {
        if (_done)
            return;

        RaycastHit hit;
        if(Physics.Raycast(transform.position, _centerEye.position - transform.position, out hit, 10.0f))
        {
            Debug.Log(hit.collider.tag);
            if (hit.collider.tag == "MainCamera" || hit.collider.tag == "Player")
                _playerInSight = true;
        }
        Debug.DrawRay(transform.position, _centerEye.position - transform.position);
        Debug.Log(_playerInSight + " " + Vector3.Angle(_centerEye.transform.forward, transform.forward));

        if (_playerInSight && Vector3.Angle(_centerEye.transform.forward, transform.forward) <= _angle)
        {
            foreach(AudioEvent a in _audioEvents)
                a.PlaySound();

            _done = true;
        }
    }
}

[System.Serializable]
public class AudioEvent
{
    [FMODUnity.EventRef]
    [SerializeField] private string ObjectEvent;
    private FMOD.Studio.EventInstance _eventInstance;

    [SerializeField] private Transform _audioSource;

    private bool _done = false;

    public void PlaySound()
    {
        Debug.Log("Start");
        if (!_done)
        {
            _eventInstance = FMODUnity.RuntimeManager.CreateInstance(ObjectEvent);
            _eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_audioSource));

            _eventInstance.start();
            _done = true;
        }
    }
}
