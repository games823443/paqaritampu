﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class TriggerSoundEventOneShot : MonoBehaviour
{
    [FMODUnity.EventRef]
    [SerializeField] private string ObjectEvent;
    private FMOD.Studio.EventInstance _eventInstance;

    [SerializeField] private Transform _audioSource;

    private bool _done = false;

    private void Start()
    {
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Collider>().isTrigger = true;
    }

    public void PlaySound()
    {
        _eventInstance = FMODUnity.RuntimeManager.CreateInstance(ObjectEvent);

        if(_audioSource != null)
            _eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_audioSource));
        else
            _eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));


        _eventInstance.start();
        _done = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !_done)
            PlaySound();
    }
}
