﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager s_instance;

    [Header("UI")]
    [SerializeField] private TMP_Text DebugText;

    [Header("Player")]
    [SerializeField] private GameObject _centerEye;
    [SerializeField] private GameObject _leftHand;
    [SerializeField] private GameObject _rightHand;

    [Space()]
    [SerializeField] private GameObject _batPrefab;

    private OVRGrabber _leftGrabber;
    private OVRGrabber _rightGrabber;

    private float _health = 100.0f;

    private GameObject _grabbedObjectLeft;
    private GameObject _grabbedObjectRight;

    void Start()
    {
        s_instance = this;

        _leftGrabber = _leftHand.GetComponent<OVRGrabber>();
        _rightGrabber = _rightHand.GetComponent<OVRGrabber>();

        //SpawnBats(5, Vector3.zero);
    }

    private void Update()
    {
        //Debug.Log(_health);
        if (_health <= 0.0f)
            ResetGame();

        if (Input.GetKeyDown(KeyCode.F))
            TakeDamage(10.0f);

        if (_leftGrabber.grabbedObject != null && _leftGrabber.grabbedObject.tag == "Light")
            _leftGrabber.grabbedObject.transform.parent = null;

        if (_rightGrabber.grabbedObject != null && _rightGrabber.grabbedObject.tag == "Light")
            _rightGrabber.grabbedObject.transform.parent = null;
    }

    public void DebugMessage(string message)
    {
        DebugText.text = message;
    }

    public void ReleaseLight()
    {
        if (_leftGrabber.grabbedObject != null && _leftGrabber.grabbedObject.tag == "Light")
            _leftGrabber.ForceRelease(_leftGrabber.grabbedObject);

        if (_rightGrabber.grabbedObject != null && _rightGrabber.grabbedObject.tag == "Light")
            _rightGrabber.ForceRelease(_rightGrabber.grabbedObject);
    }

    //public void SpawnBats(int amount, Vector3 position)
    //{
    //    System.Random rnd = new System.Random();

    //    Vector3 offset;

    //    for (int i = 0; i < amount; i++)
    //    {
    //        offset = new Vector3(rnd.Next(-100, 100) / 100.0f, rnd.Next(-100, 100) / 100.0f, rnd.Next(-100, 100) / 100.0f);
    //        Instantiate(_batPrefab, position + offset, Quaternion.identity).GetComponent<Bat>().MoveToPlayer(_centerEye.transform.position);
    //    }
    //}

    public void TakeDamage(float damage)
    {
        _health -= damage;
    }

    public void ResetHealth()
    {
        _health = 100.0f;
    }

    public void ResetGame()
    {
        ResetHealth();
        MMP3.RoomManager.s_instance.ResetLevel();
    }
}
