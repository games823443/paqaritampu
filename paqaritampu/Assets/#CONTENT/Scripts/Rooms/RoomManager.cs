﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MMP3
{
    public class RoomManager : MonoBehaviour
    {
        public static RoomManager s_instance;

        [SerializeField] private GameObject[] _sections;
        [SerializeField] private GameObject[] _extraObjectsToSpawn;
        [SerializeField] private GameObject _abschnitt7;

        [SerializeField] private GameObject _whiteRoom;

        [SerializeField] private GameObject _debugCenter;

        private int _currentRoom;

        private Vector3[] _boundary;
        private Vector3 _center;
        private Vector3 _lookAt;
        private GameObject _centerCube;

        private bool _inRoom = false;

        private void Start()
        {
            s_instance = this;
            _currentRoom = 0;
            _whiteRoom.SetActive(true);
            //_sections[_currentRoom].SetActive(true);
            //_sections[_currentRoom+1].SetActive(true);

            if (OVRManager.isHmdPresent)
            {
                _boundary = OVRManager.boundary.GetGeometry(OVRBoundary.BoundaryType.PlayArea);

                if (_boundary.Length > 0)
                {
                    for (int i = 0; i < _boundary.Length; i++)
                    {
                        //GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        //cube.transform.position = _boundary[i];
                        _center.x += _boundary[i].x;
                        _center.z += _boundary[i].z;
                    }

                    _lookAt = _boundary[0] + _boundary[1];
                    _lookAt.y = -0.175f;

                    _center /= 4.0f;
                    _center.y = -0.175f;
                }
                else
                {
                    _center = Vector3.zero;
                    _center.y = -0.175f;
                    _lookAt = Vector3.forward;
                    _lookAt.y = -0.175f;
                }
                _centerCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                _centerCube.transform.position = _center;
                _centerCube.transform.LookAt(_lookAt);
                _centerCube.GetComponent<MeshRenderer>().enabled = false;
            }
            else
            {
                _center = _debugCenter.transform.position;
                _centerCube = _debugCenter;
            }

            AlignLevelWithGuardian(_whiteRoom);
            AlignLevelWithGuardian(_currentRoom);
            AlignLevelWithGuardian(_abschnitt7);
        }

        public void StartLevel()
        {
            _whiteRoom.SetActive(false);

            _sections[_currentRoom].SetActive(true);
            _sections[_currentRoom + 1].SetActive(true);
            AlignLevelWithGuardian(_currentRoom);
            AlignLevelWithGuardian(_currentRoom+1);

        }

        public void EnterRoom(int sectionID)
        {
            if (!_inRoom)
            {
                _inRoom = true;

                _currentRoom = sectionID;

                for (int i = 0; i < _sections.Length; i++)
                    _sections[i].SetActive(false);

                _sections[_currentRoom].SetActive(true);

                AlignLevelWithGuardian(_currentRoom);
                /*
                if (_currentRoom > 0)
                    _sections[_currentRoom - 1].SetActive(false);

                if (_currentRoom < _sections.Length - 1)
                    _sections[_currentRoom + 1].SetActive(false);
                    */
                if (_extraObjectsToSpawn[_currentRoom] != null && !_extraObjectsToSpawn[_currentRoom].activeSelf)
                    _extraObjectsToSpawn[_currentRoom].SetActive(true);
            }
        }

        public void SectionTransition(int section1ID, int section2ID)
        {
            _inRoom = false;

            //if (_currentRoom == section1ID)
            _sections[section2ID].SetActive(true);
            AlignLevelWithGuardian(section2ID);
            //else if (_currentRoom == section2ID)
            _sections[section1ID].SetActive(true);
            AlignLevelWithGuardian(section1ID);

            if (_extraObjectsToSpawn[_currentRoom] != null && _extraObjectsToSpawn[_currentRoom].activeSelf)
                _extraObjectsToSpawn[_currentRoom].SetActive(false);
        }

        public void ResetLevel()
        {
            for (int i = 0; i < _sections.Length; i++)
                _sections[i].SetActive(false);
            
            _currentRoom = 0;
            _sections[_currentRoom].SetActive(true);
            AlignLevelWithGuardian(_currentRoom);
        }

        public void AlignLevelWithGuardian(int id)
        {
            //if (OVRManager.isHmdPresent)
            //{
                _sections[id].transform.localPosition = _center;
                _sections[id].transform.localRotation = _centerCube.transform.rotation;
            //}
        }

        public void AlignLevelWithGuardian(GameObject level)
        {
            //if (OVRManager.isHmdPresent)
            //{
                level.transform.localPosition = _center;
                level.transform.localRotation = _centerCube.transform.rotation;
            //}
        }
    }
}
