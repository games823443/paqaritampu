﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetObjectsForTransitionActive : MonoBehaviour
{
    [SerializeField] private GameObject _objectsForTransition;
    [SerializeField] private bool _setActive;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if(_objectsForTransition.activeSelf != _setActive)
                _objectsForTransition.SetActive(_setActive);
        }
    }
}
