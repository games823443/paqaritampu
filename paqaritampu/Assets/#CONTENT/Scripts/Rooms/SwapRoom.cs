﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapRoom : MonoBehaviour
{
    [SerializeField] private GameObject _from;
    [SerializeField] private GameObject _to;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _from.SetActive(false);
            MMP3.RoomManager.s_instance.AlignLevelWithGuardian(_to.GetComponent<Room>().ID);
            _to.SetActive(true);
        }
    }
}
