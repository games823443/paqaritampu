﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiddleManager : MonoBehaviour
{
    [SerializeField] private GameObject _leftHinge;
    [SerializeField] private GameObject _rightHinge;

    [SerializeField] private GameObject _guardian;

    [SerializeField] private FMODUnity.StudioEventEmitter[] _sounds;

    [FMODUnity.EventRef]
    [SerializeField] private string _buttonEvent;
    private FMOD.Studio.EventInstance _buttonSound;

    private int _buttonsPressed = 0;
    public bool RiddleSolved = false;
    private float _doorAngle = 0.0f;
    private Stack<ButtonForRiddles> _pressedButtons;

    private int _lastSequence = 0;

    private bool _soundPlayed = false; //play sound only once when is solved is pressed in inspector (for debug only)

    bool _test = false;

    private void Start()
    {
        _pressedButtons = new Stack<ButtonForRiddles>();
        _buttonSound = FMODUnity.RuntimeManager.CreateInstance(_buttonEvent);
        _buttonSound.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
    }

    public void ButtonPressed(ButtonForRiddles button, int sequence)
    {
        if ((sequence == 0 || sequence - _lastSequence != 1) && _pressedButtons.Count > 0) //if wrong sequence -> reset buttons
        {
            while(_pressedButtons.Count > 0)
            {
                _pressedButtons.Pop().ResetButton();
                PlayButtonSound(button.gameObject, 1);
            }

            button.ResetButton();
            PlayButtonSound(button.gameObject, 1);

            _lastSequence = 0;
            _buttonsPressed = 0;

            return;
        }

        _buttonsPressed++;

        _lastSequence = sequence;

        _pressedButtons.Push(button);

        if (_buttonsPressed == 21)
        {
            RiddleSolved = true;
            _guardian.SetActive(false);
            foreach (FMODUnity.StudioEventEmitter s in _sounds)
                s.Play();

            PlayButtonSound(gameObject, 2);
        }
    }

    private void Update()
    {
        if(RiddleSolved && _doorAngle < 90.0f)
        {
            float angle = Time.deltaTime * 20.0f;

            _rightHinge.transform.Rotate(0.0f, angle, 0.0f);
            _leftHinge.transform.Rotate(0.0f, angle * -1, 0.0f);
            _doorAngle += angle;
        }

        //play sound when solved is checked in inspector (debug only)
        if(RiddleSolved && !_soundPlayed)
        {
            foreach (FMODUnity.StudioEventEmitter s in _sounds)
                s.Play();

            _soundPlayed = true;
        }
    }

    public void PlayButtonSound(GameObject button, int track)
    {
        _buttonSound.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(button));
        _buttonSound.setParameterByName("Button_Door_Sequence", track);
        _buttonSound.start();
    }
}

