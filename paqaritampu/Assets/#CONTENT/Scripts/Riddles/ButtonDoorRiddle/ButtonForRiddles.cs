﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonForRiddles : MonoBehaviour
{
    [SerializeField] private RiddleManager _riddleManager;

    //sequence of correct buttons, 0 if wrong button
    [SerializeField] private int _sequence = 0;

    [FMODUnity.EventRef]
    [SerializeField] private string _buttonEvent;
    private FMOD.Studio.EventInstance _buttonSound;

    public bool Left = false;
    private bool _pressed = false;
    private float _pressedDepth = 0.0f;

    private void Start()
    {
        _buttonSound = FMODUnity.RuntimeManager.CreateInstance(_buttonEvent);
        _buttonSound.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            _pressed = !_pressed;
            _riddleManager.PlayButtonSound(gameObject, 0);
            //_buttonSound.setParameterByName("Button_Door_Sequence", 0);
            //_buttonSound.start();
        }

        if(_pressed && _pressedDepth < 2.5f)
        {
            MoveButton(true);
        }
        else if(!_pressed && _pressedDepth > 0.0f)
        {
            MoveButton(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("FingerTrigger"))
        {
            if (!_pressed && !_riddleManager.RiddleSolved)
            {
                _pressed = true;
                _riddleManager.ButtonPressed(this, _sequence);
                _riddleManager.PlayButtonSound(gameObject, 0);
            }
        }
    }

    private void MoveButton(bool forward)
    {
        float distance = Time.deltaTime * 5.0f;
        if (forward)
        {
            _pressedDepth += distance;

            //slowly move button
            if (Left)
                transform.localPosition += new Vector3(0.0f, 0.0f, distance);
            else
                transform.localPosition -= new Vector3(0.0f, 0.0f, distance);
        }
        else
        {
            _pressedDepth -= distance;

            //slowly reset button
            if (Left)
                transform.localPosition -= new Vector3(0.0f, 0.0f, distance);
            else
                transform.localPosition += new Vector3(0.0f, 0.0f, distance);
        }
    }

    public void ResetButton()
    {
        _pressed = false;
    }
}
