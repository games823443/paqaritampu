﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiddleLift : MonoBehaviour
{
    [SerializeField] private bool _debugStart = false;
    [SerializeField] private GameObject _effect;
    private PSMeshRendererUpdater _meshEffect;

    private GameObject _player;
    private CapsuleCollider _collider;

    private bool _startLift = false;

    private float _distance = 0.0f;

    private void Start()
    {
        _collider = GetComponent<CapsuleCollider>();
        _collider.enabled = false;

        _meshEffect = _effect.GetComponent<PSMeshRendererUpdater>();

        _effect.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (_debugStart)
        {
            ActivateLift();
        }

        if ((_startLift || _debugStart) && _distance > -4.93f)
        {
            Vector3 dist = Vector3.down * Time.deltaTime * 0.8f;

            if(_startLift)
                _player.transform.position += dist;

            transform.position += dist;

            _distance += dist.y;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _player = other.gameObject;

            StartCoroutine(WaitToStart());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            _startLift = false;
    }

    private IEnumerator WaitToStart()
    {
        yield return new WaitForSeconds(2.0f);

        _startLift = true;
    }

    public void ActivateLift()
    {
        _collider.enabled = true;
        _effect.SetActive(true);
        _meshEffect.IsActive = true;
        _meshEffect.UpdateMeshEffect();
        //add effect
    }
}
