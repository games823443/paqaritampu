﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObjectKind { AXE, SICKLE, NONE}

public class StatuaryBehaviour : MonoBehaviour
{
    [SerializeField] private Transform _snapPoint;
    [SerializeField] private OfferingRiddleManager _riddleManager;
    [SerializeField] private ObjectKind _correctObject;

    private StatuaryObject _currentObject;
    private bool _inHand = false;
    private bool _correct = false;
    private ObjectKind _objectKind = ObjectKind.NONE;
    
    // Update is called once per frame
    void Update()
    {
        if (_correct)
            return;

        if (_objectKind != ObjectKind.NONE && !_currentObject.IsGrabbed())
        {
            _inHand = true;

            if (_objectKind == _correctObject)
            {
                _correct = true;
                _currentObject.Correct();

                _currentObject.transform.position = _snapPoint.position;
                _currentObject.transform.rotation = _snapPoint.rotation;
                _currentObject.GetComponent<Rigidbody>().isKinematic = true;

                _riddleManager.ObjectPlaced(_objectKind);
            }
        }

        if(_inHand)
        {
            _currentObject.transform.position = _snapPoint.position;
            _currentObject.transform.rotation = _snapPoint.rotation;

            if (_currentObject.IsGrabbed())
                _inHand = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_correct)
            return;

        if(other.CompareTag("Axe") && _objectKind == ObjectKind.NONE)
        {
            _objectKind = ObjectKind.AXE;
            _currentObject = other.GetComponent<StatuaryObject>();
        }
        else if(other.CompareTag("Sickle") && _objectKind == ObjectKind.NONE)
        {
            _objectKind = ObjectKind.SICKLE;
            _currentObject = other.GetComponent<StatuaryObject>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (_correct)
            return;

        if (_currentObject != null && _currentObject.IsGrabbed())
        {
            if (other.CompareTag("Axe") && _objectKind == ObjectKind.AXE)
            {
                _objectKind = ObjectKind.NONE;
                _currentObject = null;
            }
            else if (other.CompareTag("Sickle") && _objectKind == ObjectKind.SICKLE)
            {
                _objectKind = ObjectKind.NONE;
                _currentObject = null;
            }
        }
    }
}
