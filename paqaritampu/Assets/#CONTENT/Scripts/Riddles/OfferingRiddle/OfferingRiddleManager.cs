﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OfferingRiddleManager : MonoBehaviour
{
    [SerializeField] private RiddleLift _lift;
    private int _correctObjects;
    private int[] _order;

    private bool _axeCorrectlyPlaced = false;
    private bool _sickleCorrectlyPlaced = false;

    private bool _liftActivated = false;
    
    private void Start()
    {
        _order = new int[] { -1, -1, -1, -1, -1, -1};
    }

    private void Update()
    {
        if (!_liftActivated && _correctObjects == 6 && _axeCorrectlyPlaced && _sickleCorrectlyPlaced)
        {
            _lift.ActivateLift();
            _liftActivated = true;
        }
    }

    public void AddCorrectObject(int id)
    {
        if (id == 0 || _order[id - 1] != -1)
        {
            _order[id] = id;
            _correctObjects++;
        }
        else
        {
            _order = new int[] { -1, -1, -1, -1, -1, -1 };
            _correctObjects = 0;
        }
    }

    public void RemoveCorrectObject(int id)
    {
        _order[id] = -1;

        if(_correctObjects > 0)
            _correctObjects--;
    }

    public void ObjectPlaced(ObjectKind objectKind)
    {
        if (objectKind == ObjectKind.AXE)
            _axeCorrectlyPlaced = true;
        else if (objectKind == ObjectKind.SICKLE)
            _sickleCorrectlyPlaced = true;
    }
}
