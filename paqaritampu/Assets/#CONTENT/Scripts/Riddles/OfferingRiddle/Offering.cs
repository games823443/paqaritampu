﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Offering : MonoBehaviour
{
    [SerializeField] private int _id;

    public int GetID()
    {
        return _id;
    }
}
