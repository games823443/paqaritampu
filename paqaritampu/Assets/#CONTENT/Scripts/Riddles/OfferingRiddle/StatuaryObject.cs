﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatuaryObject : MonoBehaviour
{
    private OVRGrabbable _grabbable;

    // Start is called before the first frame update
    void Start()
    {
        _grabbable = GetComponent<OVRGrabbable>();
    }

    public bool IsGrabbed()
    {
        return _grabbable.isGrabbed;
    }

    public void Correct()
    {
        _grabbable.enabled = false;
    }
}
