﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OfferingBowl : MonoBehaviour
{
    [SerializeField] private int _id;
    [SerializeField] private OfferingRiddleManager _offeringManager;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Offering") || other.CompareTag("Collectible") || other.CompareTag("Book"))
        {
            Debug.Log(other.GetComponent<Offering>().GetID());
            if (other.GetComponent<Offering>().GetID() == _id)
                _offeringManager.AddCorrectObject(_id);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Offering") || other.CompareTag("Collectible") || other.CompareTag("Book"))
        {
            if (other.GetComponent<Offering>().GetID() == _id)
                _offeringManager.RemoveCorrectObject(_id);
        }
    }
}
