﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPositionCorrection : MonoBehaviour
{
    [SerializeField] private Transform _floor;
    private float _yOffset;
    private Collider _collider;

    // Start is called before the first frame update
    void Start()
    {
        _yOffset = _floor.position.y - transform.position.y;
        _collider = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Magnitude(transform.position - _floor.position) > 0.4f)
        {
            transform.position = _floor.position;
        }

        transform.localPosition = new Vector3(transform.localPosition.x, _yOffset, transform.localPosition.z);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Boardgame")
            Physics.IgnoreCollision(collision.collider, _collider);
    }
}
