﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardgameSavepointTrigger : MonoBehaviour
{
    [SerializeField] private Boardgame _boardgame;
    private bool _isTriggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if(!_isTriggered && other.CompareTag("Ball"))
        {
            _isTriggered = true;
            _boardgame.BallReachedGoal(other.gameObject);
        }
    }
}
