﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boardgame : MonoBehaviour
{
    [SerializeField] private GameObject _ball1;
    [SerializeField] private GameObject _ball2;

    [SerializeField] private GameObject _doorLeft;
    [SerializeField] private GameObject _doorRight;

    [SerializeField] private GameObject _key;
    [SerializeField] private bool _solved;
    
    private int _ballsDone = 0;
    private float _doorAngle = 0.0f;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((_solved || _ballsDone == 2) && _doorAngle < 90.0f)
        {
            float angle = Time.deltaTime * 20.0f;

            _doorLeft.transform.Rotate(0.0f, 0.0f, angle);
            _doorRight.transform.Rotate(0.0f, 0.0f, angle * -1);
            _doorAngle += angle;
        }
        else if (_doorAngle >= 90.0f)
            _key.SetActive(true);

    }

    public void BallReachedGoal(GameObject ball)
    {
        _ballsDone++;

        if (ball == _ball1)
            _ball1.GetComponent<Rigidbody>().isKinematic = true;
        else
            _ball2.GetComponent<Rigidbody>().isKinematic = true;
    }
}
