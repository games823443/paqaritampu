﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateEffect : MonoBehaviour
{
    private PSMeshRendererUpdater _psMeshRenderer;

    private void Start()
    {
        _psMeshRenderer = GetComponent<PSMeshRendererUpdater>();
    }

    private void OnEnable()
    {
        StartCoroutine(WaitToUpdate());
    }

    private IEnumerator WaitToUpdate()
    {
        yield return new WaitForEndOfFrame();
        _psMeshRenderer.UpdateMeshEffect();
    }
}
