﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteRoomTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            MMP3.RoomManager.s_instance.StartLevel();
        }
    }
}
