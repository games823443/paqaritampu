﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianWhiteRoom : MonoBehaviour
{
    [SerializeField] private GameObject _level1;
    [SerializeField] private GameObject _parent;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _parent.SetActive(false);
            _level1.SetActive(true);
        }
    }
}
