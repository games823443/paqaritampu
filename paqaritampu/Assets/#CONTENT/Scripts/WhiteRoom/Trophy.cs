﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trophy : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed = 10.0f;
    [SerializeField] private Material _black;
    [SerializeField] private bool _collected = false;
    private Material _ownMaterial;
    private Renderer _renderer;

    // Start is called before the first frame update
    void Start()
    {
        _renderer = GetComponent<Renderer>();
        _ownMaterial = _renderer.material;
        _renderer.material = _black;
    }

    // Update is called once per frame
    void Update()
    {
        if(_collected)
        {
            TrophyCollected();
            _collected = false;
        }

        transform.Rotate(Vector3.up * Time.deltaTime * _rotationSpeed);
    }

    public void TrophyCollected()
    {
        _renderer.material = _ownMaterial;
    }
}
