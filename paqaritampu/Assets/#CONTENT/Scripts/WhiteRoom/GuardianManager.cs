﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianManager : MonoBehaviour
{
    [SerializeField] private GameObject _whiteRoom;
    [SerializeField] private GameObject _level1;
    [SerializeField] private GameObject _returnTrigger;
    [SerializeField] private LayerMask _layerMask;
    [SerializeField] private bool _isActive;
    

    private void ActivateGuardian(Collision collision)
    {
        Vector3 returnPos = Vector3.zero;
        RaycastHit hit;

        Debug.DrawRay(collision.GetContact(0).point, collision.GetContact(0).normal, Color.red, 10.0f);

        if (Physics.Raycast(collision.GetContact(0).point, collision.GetContact(0).normal, out hit, 10.0f, _layerMask))
        {
            returnPos = (collision.GetContact(0).point + hit.point) / 2.0f;
            Debug.DrawRay(hit.point, hit.normal, Color.red, 10.0f);
        }
        else
        {
            returnPos = collision.GetContact(0).point + collision.GetContact(0).normal;
        }

        if(Physics.Raycast(returnPos, Vector3.down, out hit, 5.0f))
        {
            returnPos = hit.point;
        }

        _level1.SetActive(false);
        _whiteRoom.transform.position = new Vector3(transform.position.x, returnPos.y, transform.position.z);
             
        //_returnTrigger.transform.position = new Vector3(returnPos.x, 0.0f, returnPos.z);
        _returnTrigger.transform.position = returnPos;
        //_returnCube.transform.position = new Vector3(returnPos.x, 0.0f, returnPos.z); ;
        _returnTrigger.SetActive(false);
        StartCoroutine(WaitToActivateTrigger());

        _whiteRoom.SetActive(true);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_isActive && collision.gameObject.tag == "Guardian")
            ActivateGuardian(collision);
    }

    private IEnumerator WaitToActivateTrigger()
    {
        yield return new WaitForSeconds(1.0f);
        _returnTrigger.SetActive(true);
    }
}
