﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackpackManager : MonoBehaviour
{
    [SerializeField] private Transform _snapPoint;
    private List<GameObject> _content;
    private System.Random _rnd = new System.Random();
    private float _rndOffsetX = 0.0f;
    private float _rndOffsetZ = 0.0f;

    private GameObject _book;
    private bool _bookInside = false;
    private bool _bookPlaced = false;
    private Book _bookComp;
    private Rigidbody _bookRigidbody;
    private OVRGrabbable _grabbable;

    void Awake()
    {
        _content = new List<GameObject>();
    }

    private void Update()
    {
        if(_bookInside)
        {
            if(!_bookPlaced && !_grabbable.isGrabbed)
            {
                _book.transform.parent = transform;
                _book.transform.localPosition = _snapPoint.localPosition;
                _book.transform.localRotation = _snapPoint.localRotation;
                if(_bookRigidbody == null)
                    _bookRigidbody = _book.GetComponent<Rigidbody>();

                _bookRigidbody.isKinematic = true;

                if (_bookComp == null)
                    _bookComp = _book.GetComponent<Book>();
                        
                _bookComp._inBackpack = true;
                _bookPlaced = true;
            }
        }

        if(_bookPlaced && _grabbable.isGrabbed)
        {
            _book.transform.parent = null;
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Collectible")
        {
            if (_content.Count < 10)
            {
                if(_content.Find(x => x == other.gameObject) == null)
                    _content.Add(other.gameObject);
            }
        }

        if(other.tag == "Book")
        {
            _book = other.gameObject;
            _bookInside = true;
            _grabbable = other.GetComponent<OVRGrabbable>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Collectible")
            _content.Remove(other.gameObject);

        if (other.tag == "Book")
        {
            _book = null;
            _bookPlaced = false;
            _bookInside = false;
            _grabbable = null;
            _bookComp._inBackpack = false;
        }
    }

    public void PlaceItems()
    {
        float offset = 0.05f;
        if (_content.Count > 0)
        {
            foreach (GameObject go in _content)
            {
                _rndOffsetX = _rnd.Next(-15, 15) / 100.0f;
                _rndOffsetZ = _rnd.Next(-15, 15) / 100.0f;

                go.SetActive(true);                
                go.transform.position = transform.position + new Vector3(0.0f, offset, 0.0f) + transform.forward * 0.25f + transform.forward * _rndOffsetZ + transform.right * _rndOffsetX;
                offset += 0.05f;
            }
        }
    }

    public void HideItems()
    {
        if (_content.Count > 0)
        {
            foreach (GameObject go in _content)
            {
                go.SetActive(false);
            }
        }
    }
}
