﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick : MonoBehaviour
{
    private OVRGrabbable _grabbable;
    private Rigidbody _rigidbody;

    private bool _hasBeenGrabbed = false;

    [FMODUnity.EventRef]
    [SerializeField] private string StickEvent;
    FMOD.Studio.EventInstance _stickEventInstance;

    void Start()
    {
        _grabbable = GetComponent<OVRGrabbable>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (_grabbable.isGrabbed)
        {
            if (!_hasBeenGrabbed)
            {
                _hasBeenGrabbed = true;
                transform.parent = null;
                _rigidbody.useGravity = true;

                _stickEventInstance = FMODUnity.RuntimeManager.CreateInstance(StickEvent);
                _stickEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
                _stickEventInstance.start();
            }

            _rigidbody.isKinematic = true;
        }
        else if (!_grabbable.isGrabbed && _hasBeenGrabbed)
            _rigidbody.isKinematic = false;
    }
}
