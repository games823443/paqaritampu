﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Backpack : MonoBehaviour
{
    [SerializeField] private GameObject _backpack;
    [SerializeField] private GameObject _eyeAnchor;
    [SerializeField] private GameObject _leftHand;
    [SerializeField] private GameObject _rightHand;
    
    private BackpackManager _backpackManager;

    private Vector3 _yOffset;

    private GameObject _currentHand;
    private bool _left;
    private bool _isHidden;

    private Rigidbody _backpackRigidbody;

    [FMODUnity.EventRef]
    [SerializeField] private string BackpackEvent;
    FMOD.Studio.EventInstance _backpackEventInstance;

    private void Start()
    {
        _backpackManager = _backpack.GetComponent<BackpackManager>();
        _backpack.SetActive(false);

        _yOffset = new Vector3(0.0f, -0.4f, 0.1f);

        _currentHand = null;
        _isHidden = true;
        _backpackRigidbody = _backpack.GetComponent<Rigidbody>();
    }

    void Update()
    {
        //if hand is null -> check if one hand is behind head and presses hand trigger
        if (_isHidden && _currentHand == null)
        {
            Vector3 heading = _leftHand.transform.position - _eyeAnchor.transform.position;

            if (Vector3.Dot(_eyeAnchor.transform.forward, heading) < 0.0f && Vector3.Magnitude(_leftHand.transform.position - _eyeAnchor.transform.position) < 0.4f && OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger))
            {
                _backpack.SetActive(true);
                _currentHand = _leftHand;
                _left = true;
                _isHidden = false;

                _backpackEventInstance = FMODUnity.RuntimeManager.CreateInstance(BackpackEvent);
                _backpackEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_currentHand));
                _backpackEventInstance.start();
            }

            heading = _rightHand.transform.position - _eyeAnchor.transform.position;

            if (Vector3.Dot(_eyeAnchor.transform.forward, heading) < 0.0f && Vector3.Magnitude(_rightHand.transform.position - _eyeAnchor.transform.position) < 0.4f && OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger))
            {
                _backpack.SetActive(true);
                _currentHand = _rightHand;
                _left = false;
                _isHidden = false;

                _backpackEventInstance = FMODUnity.RuntimeManager.CreateInstance(BackpackEvent);
                _backpackEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_currentHand));
                _backpackEventInstance.start();
            }
        }
        else {
            //if one hand grabs backpack -> place backpack at hands position
            if (_currentHand != null)
            {
                _backpack.transform.position = _currentHand.transform.position;
                _backpack.transform.rotation = _currentHand.transform.rotation;

                if (_left)
                    _backpack.transform.Rotate(new Vector3(-90.0f, 0.0f, -90.0f));
                else
                    _backpack.transform.Rotate(new Vector3(90.0f, 0.0f, 90.0f));
            }

            //_backpack.transform.localPosition += _yOffset;
            _backpackManager.PlaceItems();

            if((_left && OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger)) || (!_left && OVRInput.GetUp(OVRInput.Button.SecondaryHandTrigger)))
            {
                _backpackEventInstance = FMODUnity.RuntimeManager.CreateInstance(BackpackEvent);
                _backpackEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_currentHand));
                _backpackEventInstance.start();

                //if button is released -> place backpack in scene
                _currentHand = null;

                _backpackRigidbody.isKinematic = false;

                //if button is release behind head -> hide backpack
                Vector3 heading = _leftHand.transform.position - _eyeAnchor.transform.position;
                if (_left && Vector3.Dot(_eyeAnchor.transform.forward, heading) < 0.0f)
                {
                    _backpack.SetActive(false);
                    _backpackManager.HideItems();
                    _isHidden = true;
                    Debug.Log("is hidden true left");
                }

                heading = _rightHand.transform.position - _eyeAnchor.transform.position;
                if (!_left && Vector3.Dot(_eyeAnchor.transform.forward, heading) < 0.0f)
                {
                    _backpack.SetActive(false);
                    _backpackManager.HideItems();
                    _isHidden = true;
                    Debug.Log("is hidden true right");
                }
            }
        }
    }

    public void PickUpBackpack(GameObject hand)
    {
        if (hand == _leftHand)
        {
            _left = true;
            _currentHand = _leftHand;
        }
        else
        {
            _left = false;
            _currentHand = _rightHand;
        }

        _backpackRigidbody.isKinematic = true;
    }
}
