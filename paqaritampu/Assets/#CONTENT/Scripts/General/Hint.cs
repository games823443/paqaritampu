﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hint : MonoBehaviour
{
    [SerializeField] private Book _book;

    private OVRGrabbable _grabbable;
    private Material _material;
    private Rigidbody _rigidbody;

    private bool _hasBeenGrabbed = false;
    private bool _hasBeenAddedToBook = false;

    // Start is called before the first frame update
    void Start()
    {
        _grabbable = GetComponent<OVRGrabbable>();
        _material = GetComponent<Renderer>().material;
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_hasBeenGrabbed && _grabbable.isGrabbed)
            _hasBeenGrabbed = true;

        if (_hasBeenGrabbed && !_grabbable.isGrabbed && !_hasBeenAddedToBook)
        {
            _hasBeenAddedToBook = true;
            _book.AddPage(_material);
            _rigidbody.isKinematic = false;
        }
    }
}
