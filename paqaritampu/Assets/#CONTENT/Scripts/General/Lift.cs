﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lift : MonoBehaviour
{
    private GameObject _player;

    private bool _startLift = false;
    private bool _liftStarted = false;

    private float _distance = 0.0f;

    [Header("FMODEvents")]
    [FMODUnity.EventRef]
    [SerializeField] private string Wheel1Event;
    FMOD.Studio.EventInstance _wheel1EventInstance;
    [FMODUnity.EventRef]
    [SerializeField] private string Wheel2Event;
    FMOD.Studio.EventInstance _wheel2EventInstance;
    [FMODUnity.EventRef]
    [SerializeField] private string Wheel3Event;
    FMOD.Studio.EventInstance _wheel3EventInstance;
    [FMODUnity.EventRef]
    [SerializeField] private string Wheel4Event;
    FMOD.Studio.EventInstance _wheel4EventInstance;
    [FMODUnity.EventRef]
    [SerializeField] private string FloorEvent;
    FMOD.Studio.EventInstance _floorEventInstance;

    [Header("Wheel Objects")]
    [SerializeField] private GameObject _wheel1;
    [SerializeField] private GameObject _wheel2;
    [SerializeField] private GameObject _wheel3;
    [SerializeField] private GameObject _wheel4;
    [SerializeField] private GameObject _floor;


    void Update()
    {
        if (_startLift && _distance > -26.6f)
        {
            Vector3 dist = Vector3.down * Time.deltaTime * 0.8f;
            _player.transform.position += dist;
            transform.parent.position += dist;
            _distance += dist.y;

            _wheel1EventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_wheel1));
            _wheel2EventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_wheel2));
            _wheel3EventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_wheel3));
            _wheel4EventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_wheel4));
            _floorEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_floor));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _player = other.gameObject;
            if (!_liftStarted)
            {
                StartCoroutine(WaitToStart());
                _liftStarted = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            _startLift = false;
    }

    private IEnumerator WaitToStart()
    {
        yield return new WaitForSeconds(2.0f);

        _startLift = true;

        _wheel1EventInstance = FMODUnity.RuntimeManager.CreateInstance(Wheel1Event);
        _wheel1EventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_wheel1));

        _wheel2EventInstance = FMODUnity.RuntimeManager.CreateInstance(Wheel2Event);
        _wheel2EventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_wheel2));

        _wheel3EventInstance = FMODUnity.RuntimeManager.CreateInstance(Wheel3Event);
        _wheel3EventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_wheel3));

        _wheel4EventInstance = FMODUnity.RuntimeManager.CreateInstance(Wheel4Event);
        _wheel4EventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_wheel4));

        _floorEventInstance = FMODUnity.RuntimeManager.CreateInstance(FloorEvent);
        _floorEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_floor));

        _wheel1EventInstance.start();
        _wheel2EventInstance.start();
        _wheel3EventInstance.start();
        _wheel4EventInstance.start();
        _floorEventInstance.start();
    }
}
