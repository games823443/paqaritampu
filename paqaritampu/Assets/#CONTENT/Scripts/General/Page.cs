﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Page : MonoBehaviour
{
    [SerializeField] private Book _book;
    private float _angle;
    private bool _changePage = false;
    //private bool _closeBook = false;
    private bool _fromRightToLeft;

    private void Update()
    {
        if (_changePage)
        {
            if (_fromRightToLeft && _angle < 180.0f)
            {
                _angle += Time.deltaTime * 100.0f;

                if (_angle > 180.0f)
                {
                    transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 179.9f);
                    _book.PageChangeFinished();
                }
                else
                    transform.Rotate(0.0f, 0.0f, Time.deltaTime * 100.0f);
            }
            else if(!_fromRightToLeft && _angle > 0.0f)
            {
                _angle -= Time.deltaTime * 100.0f;

                if (_angle < 0.0f)
                {
                    transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                    _book.PageChangeFinished();
                }
                else
                    transform.Rotate(0.0f, 0.0f, Time.deltaTime * -100.0f);
            }
        }

        //if(_closeBook)
        //{
        //    _angle -= Time.deltaTime * 100.0f;

        //    if (_angle < 90.0f)
        //    {
        //        transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 90.0f);
        //        _book.PageChangeFinished();
        //    }
        //    else
        //        transform.Rotate(0.0f, 0.0f, Time.deltaTime * -100.0f);
        //}
    }

    public void ChangePage(bool rtl)
    {
        _fromRightToLeft = rtl;
        _changePage = true;

        if (rtl)
            _angle = 0.0f;
        else
            _angle = 180.0f;
    }

    //public void CloseBook()
    //{
    //    _closeBook = true;
    //}
}
