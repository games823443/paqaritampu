﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneGrab : MonoBehaviour
{
    [SerializeField] private Transform _grabTarget;
    [SerializeField] private Transform _snapPointLeft;
    [SerializeField] private Transform _snapPointRight;
    [SerializeField] private GameObject _phone;
    [SerializeField] private GameObject _leftHand;
    [SerializeField] private GameObject _rightHand;

    private GameObject _currentHand;
    private bool _left;
    private bool _isHidden;
    private Rigidbody _rigidbody;
    private Phone _phoneComponent;

    // Start is called before the first frame update
    void Start()
    {
        _currentHand = null;
        _isHidden = true;

        _phone.SetActive(false);
        _rigidbody = GetComponent<Rigidbody>();
        _phoneComponent = _phone.GetComponent<Phone>();
    }

    // Update is called once per frame
    void Update()
    {
        _grabTarget.transform.position = transform.position - Vector3.up*0.7f;
        _grabTarget.transform.rotation = transform.rotation;

        if (_isHidden && _currentHand == null)
        {
            Vector3 heading = _leftHand.transform.position - _grabTarget.transform.position;

            if (Vector3.Dot(_grabTarget.transform.forward, heading) < 0.0f && Vector3.Magnitude(_leftHand.transform.position - _grabTarget.position) < 0.4f && OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger))
            {
                _phone.SetActive(true);
                _currentHand = _leftHand;
                _left = true;
                _isHidden = false;
            }

            heading = _rightHand.transform.position - _grabTarget.transform.position;

            if (Vector3.Dot(_grabTarget.transform.forward, heading) < 0.0f && Vector3.Magnitude(_rightHand.transform.position - _grabTarget.position) < 0.4f && OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger))
            {
                _phone.SetActive(true);
                _currentHand = _rightHand;
                _left = false;
                _isHidden = false;
            }
        }
        else
        {
            //if one hand grabs phone -> place phone at hands position
            if (_currentHand != null)
            {
                //_phone.transform.position = _currentHand.transform.position;
                //_phone.transform.rotation = _currentHand.transform.rotation;

                if (_left)
                {
                    _phone.transform.position = _snapPointLeft.position;
                    _phone.transform.rotation = _snapPointLeft.rotation;
                }
                else
                {
                    _phone.transform.position = _snapPointRight.position;
                    _phone.transform.rotation = _snapPointRight.rotation;
                }

                if ((_left && OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger)) || (!_left && OVRInput.GetUp(OVRInput.Button.SecondaryHandTrigger)))
                {
                    //if button is released -> place phone in scene
                    _currentHand = null;

                    _rigidbody.isKinematic = false;

                    //if button is release behind head -> hide backpack
                    Vector3 heading = _leftHand.transform.position - _grabTarget.transform.position;
                    if (_left && Vector3.Dot(_grabTarget.transform.forward, heading) < 0.0f)
                    {
                        _phone.SetActive(false);
                        _isHidden = true;
                    }

                    heading = _rightHand.transform.position - _grabTarget.transform.position;
                    if (!_left && Vector3.Dot(_grabTarget.transform.forward, heading) < 0.0f)
                    {
                        _phone.SetActive(false);
                        _isHidden = true;
                    }
                }
            }
        }

        //Debug.Log("Left: " + _left + " CurrentHand: " + _currentHand + " IsHidden: " + _isHidden);

        _phoneComponent.GrabbedBy = _currentHand;
    }

    public void PickUpPhone(GameObject hand)
    {
        if (hand == _leftHand)
        {
            _left = true;
            _currentHand = _leftHand;
        }
        else
        {
            _left = false;
            _currentHand = _rightHand;
        }

        _rigidbody.isKinematic = true;

        _phoneComponent.GrabbedBy = _currentHand;
    }
}
