﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickCollider : MonoBehaviour
{
    private GameObject _light;

    [FMODUnity.EventRef]
    [SerializeField] private string StickEvent;
    FMOD.Studio.EventInstance _stickEventInstance;

    void Update()
    {
        if (_light != null)
        {
            _light.transform.position = transform.position;
            _light.transform.rotation = transform.rotation;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Light")
        {
            GameManager.s_instance.ReleaseLight();

            _light = other.gameObject;

            if (_light.GetComponent<MeshCollider>() != null)
                _light.GetComponent<MeshCollider>().enabled = false;
            else if (_light.GetComponent<CapsuleCollider>() != null)
                _light.GetComponent<CapsuleCollider>().enabled = false;
            _light.transform.parent = null;

            _stickEventInstance = FMODUnity.RuntimeManager.CreateInstance(StickEvent);
            _stickEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
            _stickEventInstance.start();
        }
    }
}
