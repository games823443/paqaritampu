﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Controllables;

public class Book : MonoBehaviour
{
    [SerializeField] private GameObject[] _pages;
    [SerializeField] private GameObject _eye;

    [SerializeField] private GameObject _lWing;
    [SerializeField] private GameObject _rWing;

    private Stack<GameObject> _pagesRight;
    private Stack<GameObject> _pagesLeft;

    private OVRGrabber _grabber;
    private OVRGrabbable _grabbable;
    
    private bool _isOpen = true;
    private bool _open = false;
    private bool _canChangePage = true;

    private GameObject _objectToHide;
    private Vector3 _lastControllerDistance = Vector3.zero;

    private int _pageCounter = 0;

    private int _posToAddHint;

    private Rigidbody _rigidbody;
    private bool _firstGrab;
    public bool _inBackpack = false;

    void Start()
    {
        _grabbable = GetComponent<OVRGrabbable>();
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.isKinematic = true;

        _pagesRight = new Stack<GameObject>();
        _pagesLeft = new Stack<GameObject>();  
        
        for(int i = 0; i < _pages.Length; i++)
        {
            _pagesRight.Push(_pages[i]);
            _pages[i].SetActive(false);
        }

        _isOpen = true;
        _pagesRight.Peek().SetActive(true);

        _posToAddHint = _pages.Length - 1;
    }

    void Update()
    {
        if (_grabbable.isGrabbed)
        {
            if(!_firstGrab)
            {
                //_rigidbody.isKinematic = false;
                transform.parent = null;
                _firstGrab = true;
            }

            _grabber = _grabbable.grabbedBy;

            if (!_isOpen)
                _open = true;

            OVRInput.Controller controller = OVRInput.Controller.RTouch;

            if (_grabber.name == "CustomHandRight")
                controller = OVRInput.Controller.LTouch;
            
            if (_open)
            {
                if (_rWing.transform.localEulerAngles.z - 360.0f < 0.0f && _lWing.transform.localEulerAngles.z > 0.0f && _lWing.transform.localEulerAngles.z < 100.0f)
                {
                    _rWing.transform.Rotate(0.0f, 0.0f, Time.deltaTime * 50.0f);
                    _lWing.transform.Rotate(0.0f, 0.0f, Time.deltaTime * -50.0f);

                    for (int i = 0; i < _pages.Length; i++)
                        _pages[i].transform.Rotate(0.0f, 0.0f, Time.deltaTime * -50.0f);
                }
                else
                {
                    _rWing.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                    _lWing.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);

                    for (int i = 0; i < _pages.Length; i++)
                        _pages[i].transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);

                    _open = false;
                    _isOpen = true;
                }
            }

            if (_isOpen && _canChangePage && Vector3.Magnitude(OVRInput.GetLocalControllerVelocity(controller)) > 0.6f)
            {
                float angle = Vector3.Angle(new Vector3(0.0f, 1.0f, 0.0f), OVRInput.GetLocalControllerVelocity(controller));

                if (angle > 85.0f && angle < 95.0f)
                {
                    float direction = Vector3.SignedAngle(new Vector3(0.0f, 1.0f, 0.0f), OVRInput.GetLocalControllerVelocity(controller), _eye.transform.forward);

                    if (direction > 0)
                    {
                        _canChangePage = false; 
                        StartCoroutine(WaitForNextPageChange());
                        ChangePageFromRightToLeft();
                    }
                    else if(direction < 0)
                    {
                        _canChangePage = false;
                        StartCoroutine(WaitForNextPageChange());
                        ChangePageFromLeftToRight();
                    }
                }
            }
        }
        else
        { 
            CloseBook();

            if(_firstGrab && !_inBackpack && _rigidbody.isKinematic)
                _rigidbody.isKinematic = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            ChangePageFromRightToLeft();
            _canChangePage = false;
            StartCoroutine(WaitForNextPageChange());
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            ChangePageFromLeftToRight();
            _canChangePage = false;
            StartCoroutine(WaitForNextPageChange());
        }
    }

    public void AddPage(Material mat)
    {
        //_pagesRight.Push(page);
        _pages[_posToAddHint].transform.GetChild(0).gameObject.GetComponent<Renderer>().material = mat;
        _posToAddHint--;
    }

    public void OpenBook()
    {
        _isOpen = true;
    }    

    public void CloseBook()
    {
        while (_pagesLeft.Count > 0)
        {
            _pagesLeft.Peek().transform.localRotation = Quaternion.Euler(Vector3.zero);
            _pagesLeft.Peek().SetActive(true);
            _pagesRight.Peek().SetActive(false);
            _pagesRight.Push(_pagesLeft.Pop());
        }


        if (_rWing.transform.localEulerAngles.z > -90.0f && _lWing.transform.localEulerAngles.z < 90.0f)
        {
            _rWing.transform.Rotate(0.0f, 0.0f, Time.deltaTime * -50.0f);
            _lWing.transform.Rotate(0.0f, 0.0f, Time.deltaTime * 50.0f);
            
            for (int i = 0; i < _pages.Length; i++)
                _pages[i].transform.Rotate(0.0f, 0.0f, Time.deltaTime * 50.0f);
        }
        else
        {
            _rWing.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, -90.0f);
            _lWing.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 90.0f);

            for (int i = 0; i < _pageCounter; i++)
                _pages[i].transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 90.0f);

            _isOpen = false;
            _pageCounter = 0;
        }
    }

    private void ChangePageFromLeftToRight()
    {
        if (_pagesLeft.Count > 0)
        {
            _pagesLeft.Peek().GetComponent<Page>().ChangePage(false);

            if (_pagesRight.Count > 0)
                _objectToHide = _pagesRight.Peek();

            _pagesRight.Push(_pagesLeft.Pop());

            if (_pagesLeft.Count > 0)
            {
                _pagesLeft.Peek().SetActive(true);
                _pagesLeft.Peek().transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);
            }
        }
    }

    private void ChangePageFromRightToLeft()
    {
        if (_pagesRight.Count > 0)
        {
            _pagesRight.Peek().GetComponent<Page>().ChangePage(true);

            if(_pagesLeft.Count > 0)
                _objectToHide = _pagesLeft.Peek();

            _pagesLeft.Push(_pagesRight.Pop());

            if (_pagesRight.Count > 0)
                _pagesRight.Peek().SetActive(true);
        }
    }

    private IEnumerator WaitForNextPageChange()
    {
        yield return new WaitForSeconds(2.0f);
        _canChangePage = true;
    }

    public void PageChangeFinished()
    {
        if (_objectToHide != null)
        {
            _objectToHide.SetActive(false);
            _objectToHide = null;
        }
    }
}
