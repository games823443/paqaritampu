﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour
{
    private OVRGrabbable _grabbable;
    private Rigidbody _rigidbody;

    private bool _hasBeenGrabbed = false;

    [FMODUnity.EventRef]
    [SerializeField] private string MushroomEvent;
    FMOD.Studio.EventInstance _mushroomEventInstance;

    void Start()
    {
        _grabbable = GetComponent<OVRGrabbable>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (_grabbable.isGrabbed)
        {
            if(!_hasBeenGrabbed)
            {
                _mushroomEventInstance = FMODUnity.RuntimeManager.CreateInstance(MushroomEvent);
                _mushroomEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
                _mushroomEventInstance.start();
                _hasBeenGrabbed = true;
            }
            _rigidbody.isKinematic = true;
        }
        else if (!_grabbable.isGrabbed && _hasBeenGrabbed)
            _rigidbody.isKinematic = false;
    }
}
