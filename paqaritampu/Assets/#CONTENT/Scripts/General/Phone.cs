﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phone : MonoBehaviour
{
    [SerializeField] private GameObject _light;
    [SerializeField] private GameObject _lightOn;
    [SerializeField] private GameObject _lightOff;

    [Header("Akku Symbole")]
    [SerializeField] private GameObject _akkuVoll;
    [SerializeField] private GameObject _akkuHalb;
    [SerializeField] private GameObject _akkuFastLeer;
    [SerializeField] private GameObject _akkuLeer;
    [SerializeField] private GameObject _akkuLeerNotification1;
    [SerializeField] private GameObject _akkuLeerNotification2;

    [FMODUnity.EventRef]
    [SerializeField] private string LightSwitchEvent;
    FMOD.Studio.EventInstance _lightSwitchEventInstance;

    [FMODUnity.EventRef]
    [SerializeField] private string BatteryEvent;
    FMOD.Studio.EventInstance _batteryEventInstance;

    private bool _isLightOn = false;
    private float _power = 100.0f;
    private bool _canToggle = true;
    private bool _batteryNotificationRunning = false;

    private GameObject[] _batteryStates = new GameObject[4];

    public GameObject GrabbedBy;

    private void Start()
    {
        _light.SetActive(false);
        _lightOn.SetActive(false);
        _lightOff.SetActive(true);

        _batteryStates[0] = _akkuVoll;
        _batteryStates[1] = _akkuHalb;
        _batteryStates[2] = _akkuFastLeer;
        _batteryStates[3] = _akkuLeer;
                
        SwitchBatteryState(0);

        _akkuLeerNotification1.SetActive(false);
        _akkuLeerNotification2.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
            ToggleLight();

        if (_isLightOn)
        {
            if (_power > 0.0f)
                _power -= Time.deltaTime*2;
            else
                _power = 0.0f;
        }

        if (_power < 50.0f && _power > 10.0f)
            SwitchBatteryState(1);
        else if (_power < 10.0f && _power > 0.0f)
            SwitchBatteryState(2);

        if (_power == 0.0f)
        {
            SwitchBatteryState(3);
            if (_light.activeSelf)
            {
                _light.SetActive(false);
                _isLightOn = false;

                _lightOn.SetActive(false);
                _lightOff.SetActive(true);

                if (!_batteryNotificationRunning)
                    StartCoroutine(BatteryNotification());
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "FingerTrigger" && GrabbedBy != other.gameObject /*gameObject.GetComponent<OVRGrabbable>().grabbedBy != other.transform.parent.GetComponentInChildren<OVRGrabber>()*/)
        {
            ToggleLight();
        }
    }

    private void ToggleLight()
    {
        if (_canToggle && _power > 0.0f)
        {
            _isLightOn = !_isLightOn;

            _lightSwitchEventInstance = FMODUnity.RuntimeManager.CreateInstance(LightSwitchEvent);
            _lightSwitchEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));

            _light.SetActive(_isLightOn);

            if (_isLightOn)
            {
                _lightOn.SetActive(true);
                _lightOff.SetActive(false);

                _lightSwitchEventInstance.setParameterByName("Bool", 0);
            }
            else
            {
                _lightOn.SetActive(false);
                _lightOff.SetActive(true);

                _lightSwitchEventInstance.setParameterByName("Bool", 1);
            }

            _lightSwitchEventInstance.start();

            _canToggle = false;
            StartCoroutine(LightCooldown());
        }
        else if(_power == 0.0f)
        {
            if (!_batteryNotificationRunning)
                StartCoroutine(BatteryNotification());
        }
    }

    private IEnumerator LightCooldown()
    {
        yield return new WaitForSeconds(1.0f);
        _canToggle = true;
    }

    private void SwitchBatteryState(int state)
    {
        foreach (var batteryState in _batteryStates)
            batteryState.SetActive(false);

        _batteryStates[state].SetActive(true);
    }

    private IEnumerator BatteryNotification()
    {
        _batteryNotificationRunning = true;
        
        _batteryEventInstance = FMODUnity.RuntimeManager.CreateInstance(BatteryEvent);
        _batteryEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));

        _batteryEventInstance.start();

        _akkuLeerNotification1.SetActive(true);
        yield return new WaitForSeconds(0.25f);
        _akkuLeerNotification1.SetActive(false);
        _akkuLeerNotification2.SetActive(true);
        yield return new WaitForSeconds(0.36f);
        _akkuLeerNotification1.SetActive(true);
        _akkuLeerNotification2.SetActive(false);
        yield return new WaitForSeconds(0.25f);
        _akkuLeerNotification1.SetActive(false);
        _akkuLeerNotification2.SetActive(true);
        yield return new WaitForSeconds(0.375f);
        _akkuLeerNotification1.SetActive(true);
        _akkuLeerNotification2.SetActive(false);
        yield return new WaitForSeconds(0.25f);
        _akkuLeerNotification1.SetActive(false);
        _akkuLeerNotification2.SetActive(true);
        yield return new WaitForSeconds(0.315f);
        _akkuLeerNotification2.SetActive(false);
        
        _batteryNotificationRunning = false;
    }
}
