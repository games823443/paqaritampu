﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorChildRoom : MonoBehaviour
{
    [FMODUnity.EventRef]
    [SerializeField] private string DoorEvent;
    FMOD.Studio.EventInstance _doorEventInstance;

    [SerializeField] private GameObject _leftEye;
    [SerializeField] private GameObject _forward;

    [SerializeField] private GameObject _wallToHide;
    [SerializeField] private GameObject _box;
    [SerializeField] private Transform _boxTarget;
    [SerializeField] private GameObject _blackboard;
    [SerializeField] private Transform _blackboardTarget;

    private float _openingSpeed = 8.0f;
    private float _closingSpeed = 300.0f;
    
    private bool _open = false;
    private bool _close = false;
    private bool _isOpen = false;

    private float _angle = 0.0f;
    private float _closedAngle;

    // Start is called before the first frame update
    void Start()
    {
        _closedAngle = transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
            OpenDoor();
        
        if(_open)
            Open();

        if (_close)
            Close();

        if(_isOpen)
        {
            Debug.Log(Vector3.Angle(_leftEye.transform.forward, _forward.transform.forward));
            if(Vector3.Angle(_leftEye.transform.forward, _forward.transform.forward) <= 10.0f)
            {
                CloseDoor();
                _isOpen = false;
            }
        }

    }   

    private void Open()
    {
        if(_angle < 30.0f)
            _angle += Time.deltaTime * _openingSpeed;
        else
        {
            _angle = 30.0f;
            _open = false;
            _isOpen = true;
            //Debug Only
            //CloseDoor();
        }

        transform.rotation = Quaternion.Euler(new Vector3(0.0f, _angle + _closedAngle, 0.0f));
    }

    private void Close()
    {
        if (_angle > 0.0f)
            _angle -= Time.deltaTime * _closingSpeed;
        else
        {
            _angle = 0.0f;
            _close = false;
        }

        transform.rotation = Quaternion.Euler(new Vector3(0.0f, _angle + _closedAngle, 0.0f));
    }

    private IEnumerator WaitToClose()
    {
        yield return new WaitForSeconds(1.0f);
        _close = true;
        _doorEventInstance = FMODUnity.RuntimeManager.CreateInstance(DoorEvent);
        _doorEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        _doorEventInstance.setParameterByName("Door", 1);
        _doorEventInstance.start();
    }

    public void OpenDoor()
    {
        StartCoroutine(WaitForOpen());
    }

    public void CloseDoor()
    {
        HideWall();
        StartCoroutine(WaitToClose());
    }

    private IEnumerator WaitForOpen()
    {
        yield return new WaitForSeconds(3.0f);
        _doorEventInstance = FMODUnity.RuntimeManager.CreateInstance(DoorEvent);
        _doorEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        _doorEventInstance.setParameterByName("Door", 0);
        _doorEventInstance.start();

        _open = true;

    }

    private void HideWall()
    {
        _wallToHide.SetActive(false);

        _box.transform.position = _boxTarget.position;
        _box.transform.rotation = _boxTarget.rotation;

        _blackboard.transform.position = _blackboardTarget.position;
        _blackboard.transform.rotation = _blackboardTarget.rotation;
    }
}
