﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchLight : MonoBehaviour
{
    // DECLARE FMOD AUDIO Event for Lightswitch
    [FMODUnity.EventRef]
    [SerializeField] private string LightSwitchEvent;
    FMOD.Studio.EventInstance _lightSwitchEventInstance;

    [FMODUnity.EventRef]
    [SerializeField] private string LightBreakEvent;
    FMOD.Studio.EventInstance _lightBreakEventInstance;

    [FMODUnity.EventRef]
    [SerializeField] private string ShardsEvent;
    FMOD.Studio.EventInstance _shardsEventInstance;

    private bool _isOn = false;
    private bool _isBroken = false;

    [SerializeField] private Light _light;
    [SerializeField] private GameObject _shardsTarget;

    private bool _moveUp = false;
    private bool _moveDown = false;

    private float _angle = 0.0f;
    private float _moveSpeed = 500.0f;

    // Start is called before the first frame update
    void Start()
    {
        _light.enabled = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
            ToggleLight();

        if (_moveUp)
        {
            if (_angle < 110.0f)
                _angle += Time.deltaTime * _moveSpeed;
            else
            {
                _angle = 110.0f;
                _moveUp = false;
            }

            transform.rotation = Quaternion.Euler(new Vector3(_angle, 0.0f, 0.0f));
        }

        if (_moveDown)
        {
            if (_angle > 0.0f)
                _angle -= Time.deltaTime * _moveSpeed;
            else
            {
                _angle = 0.0f;
                _moveDown = false;
            }

            transform.rotation = Quaternion.Euler(new Vector3(_angle, 0.0f, 0.0f));
        }
    }

    public void ToggleLight()
    {
        if (_moveUp || _moveDown)
            return;

        _lightSwitchEventInstance = FMODUnity.RuntimeManager.CreateInstance(LightSwitchEvent);
        _lightSwitchEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));

        if(!_isOn)
        {
            if (!_isBroken)
                StartCoroutine(breakLight());

            _lightSwitchEventInstance.setParameterByName("Bool", 0f);
            _isOn = true;
            _moveUp = true;
        }
        else
        {
            _lightSwitchEventInstance.setParameterByName("Bool", 1f);
            _isOn = false;
            _moveDown = true;
        }

        _lightSwitchEventInstance.start();


    }

    private IEnumerator breakLight()
    {
        _lightBreakEventInstance = FMODUnity.RuntimeManager.CreateInstance(LightBreakEvent);
        _lightBreakEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_light.gameObject));

        _shardsEventInstance = FMODUnity.RuntimeManager.CreateInstance(ShardsEvent);
        _shardsEventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(_shardsTarget));

        _lightBreakEventInstance.start();
        _shardsEventInstance.start();

        yield return new WaitForSeconds(0.16f);
        _light.enabled = true;
        yield return new WaitForSeconds(0.05f);
        _light.enabled = false;
        yield return new WaitForSeconds(0.247f);
        _light.enabled = true;
        yield return new WaitForSeconds(0.05f);
        _light.enabled = false;

        _isBroken = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("FingerTrigger"))
        {
            ToggleLight();
        }
    }
}
