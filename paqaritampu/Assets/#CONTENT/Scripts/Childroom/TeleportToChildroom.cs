﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportToChildroom : MonoBehaviour
{
    private OVRGrabbable _grabbable;
    [SerializeField] private OVRScreenFade _fade;
    [SerializeField] private GameObject _currentFloor;
    [SerializeField] private GameObject _childRoomFloor;
    [SerializeField] private GameObject _player;
    [SerializeField] private GameObject _level1;
    [SerializeField] private GameObject _childroom;
    [SerializeField] private HandManager _handManager;

    private bool _teleportStarted = false;

    void Start()
    {
        _grabbable = GetComponent<OVRGrabbable>();
        _childroom.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!_teleportStarted && (_grabbable.isGrabbed || Input.GetKeyDown(KeyCode.F)))
        {
            StartCoroutine(StartTeleport());
            _teleportStarted = true;
        }
    }

    private IEnumerator StartTeleport()
    {
        _fade.fadeTime = 3.0f;
        _fade.FadeOut();
        //yield return new WaitForSeconds(4.0f);
        yield return new WaitUntil(() => _fade.IsAlpha1());

        _handManager.DropEverything();
        _childroom.SetActive(true);

        Vector3 offset = _player.transform.position - _currentFloor.transform.position;
        _player.transform.position = _childRoomFloor.transform.position + offset;

        _level1.SetActive(false);

        _fade.FadeIn();
    }
}
