﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class AnimalManager : MonoBehaviour
{
    [SerializeField] private GameObject _animalPrefab;
    [SerializeField] private int _amount;
    [SerializeField] private Transform[] _path;
    [SerializeField] private float _speed = 10.0f;
    [SerializeField] private int _scattering = 100;
    [SerializeField] private int _timeOffset = 100;

    [SerializeField] private bool _startMovement = false;
    private bool _animalsInstantiated = false;
    private GameObject[] _animals;
    private System.Random rnd;

    private void Start()
    {
        rnd = new System.Random();
    }

    private void Update()
    {
        if(_startMovement)
        {
            if (!_animalsInstantiated)
            {
                _animals = new GameObject[_amount];

                Animal animalComp;

                for (int i = 0; i < _amount; i++)
                {
                    _animals[i] = Instantiate(_animalPrefab, _path[0].position, _path[0].rotation);
                    animalComp = _animals[i].AddComponent<Animal>();
                    animalComp.SetAnimalManager(this);
                    animalComp.StartMovement();
                }

                _animalsInstantiated = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!_animalsInstantiated)
            {
                _animals = new GameObject[_amount];

                Animal animalComp;

                for (int i = 0; i < 5; i++)
                {
                    _animals[i] = Instantiate(_animalPrefab, _path[0].position, _path[0].rotation);
                    animalComp = _animals[i].AddComponent<Animal>();
                    animalComp.SetAnimalManager(this);
                    animalComp.StartMovement();
                }

                _animalsInstantiated = true;
            }
        }
    }

    public Transform GetTarget(int i)
    {
        if (i < _path.Length)
            return _path[i];
        else
            return null;
    }

    public int GetPathLength()
    {
        return _path.Length;
    }

    public float GetSpeed()
    {
        return _speed;
    }

    public System.Random GetRandom()
    {
        return rnd;
    }

    public int GetScattering()
    {
        return _scattering;
    }

    public int GetTimeOffset()
    {
        return _timeOffset;
    }
}
