﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DogMovement : MonoBehaviour
{
    private NavMeshAgent _navMeshAgent;
    [SerializeField] private GameObject _player;
    [SerializeField] private GameObject _leftEye;
    [SerializeField] private GameObject _spawnPoint;

    private bool _hasWaited = false;
    private bool _idle = false;
    private bool _attack = false;
    private bool _leave = false;

    private Vector3 _playerPosInIdle = Vector3.zero;
    private Vector3 _rightIdleTarget = Vector3.zero;
    private Vector3 _leftIdleTarget = Vector3.zero;
    private Vector3 _currentIdleTarget = Vector3.zero;

    private Vector3[] _idleRoute;
    private int _routeIdx = 0;

    private Vector3 _raycastYOffset = new Vector3(0.0f, 1.0f, 0.0f);

    private float _timer = 0.0f;

    private float _distanceToPlayer = 0.0f;
    private bool _playerCloseEnough = false;
    
    void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        StartCoroutine(WaitForAttack());
        _idleRoute = new Vector3[8];
    }

    void Update()
    {
        _distanceToPlayer = Vector3.Magnitude(transform.position - _player.transform.position);

        if (_distanceToPlayer < 9.0f)
            _playerCloseEnough = true;

        if (_playerCloseEnough && _hasWaited)
        {
            if(_attack || (!_idle && !_leave))
                _navMeshAgent.destination = _player.transform.position;
            

            if (_distanceToPlayer < 2.0f && !_idle) //if dog close to player, start idleing
            {
                _idle = true;
                _playerPosInIdle = _player.transform.position;
                _navMeshAgent.destination = transform.position;
                CalcIdleRoute();
            }

            if(_idle)
            {   //if dog idles and player moves head too far -> attack
                if (Vector3.Magnitude(_player.transform.position - _playerPosInIdle) > 0.01f)
                {
                    _attack = true;
                    _idle = false;
                    _timer = 0.0f;
                }
                else
                {
                    //if (_currentIdleTarget == Vector3.zero)
                    //    _currentIdleTarget = _rightIdleTarget;

                    //if(Vector3.Magnitude(transform.position - _currentIdleTarget) < 0.5f)
                    //{ //switch idle target if current target is reached
                    //    if (_currentIdleTarget == _rightIdleTarget)
                    //        _currentIdleTarget = _leftIdleTarget;
                    //    else
                    //        _currentIdleTarget = _rightIdleTarget;
                    //}                    

                    //_navMeshAgent.destination = _currentIdleTarget;

                    _navMeshAgent.destination = _idleRoute[_routeIdx];

                    _timer += Time.deltaTime;

                    if(Vector3.Magnitude(transform.position - _idleRoute[_routeIdx]) < 0.5f)
                        _routeIdx++;

                    if (_routeIdx > 7)
                        _routeIdx = 0;
                }

                if (_timer > 10.0f)
                {
                    _leave = true;
                    _navMeshAgent.speed = 3.5f;
                }
            }

            if(_leave)
                _navMeshAgent.destination = _spawnPoint.transform.position;
        }

        if (Vector3.Magnitude(transform.position - _player.transform.position) < 1.0f)
                GameManager.s_instance.TakeDamage(100.0f);
    }

    private IEnumerator WaitForAttack()
    {
        yield return new WaitForSeconds(5.0f);
        _hasWaited = true;
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.CompareTag("StopDog"))
    //        _timer = 1.6f;
    //}

    private void CalcIdleRoute()
    {
        RaycastHit hit;

        if(Physics.Raycast(transform.position + _raycastYOffset, transform.right, out hit, 10.0f))
        {
            _rightIdleTarget = hit.point + new Vector3(-0.5f, 0.0f, 0.0f) - _raycastYOffset;
        }

        if (Physics.Raycast(transform.position + _raycastYOffset, transform.right*-1, out hit, 10.0f))
        {
            _leftIdleTarget = hit.point + new Vector3(0.5f, 0.0f, 0.0f) - _raycastYOffset;
        }

        _navMeshAgent.speed = 1.0f;

        Vector3 middle = (_rightIdleTarget + _leftIdleTarget) / 2.0f;
        Vector3 q1 = (middle + _rightIdleTarget) / 2.0f;
        Vector3 q2 = (middle + _leftIdleTarget) / 2.0f;

        GameObject quarter1 = new GameObject("quarter1");
        quarter1.transform.position = q1;
        quarter1.transform.LookAt(_rightIdleTarget);

        Vector3 q1_1 = quarter1.transform.position + quarter1.transform.right * 0.25f;
        Vector3 q1_2 = quarter1.transform.position - quarter1.transform.right * 0.25f;

        GameObject quarter2 = new GameObject("quarter2");
        quarter2.transform.position = q2;
        quarter2.transform.LookAt(_leftIdleTarget);

        Vector3 q2_1 = quarter2.transform.position + quarter2.transform.right * 0.25f;
        Vector3 q2_2 = quarter2.transform.position - quarter2.transform.right * 0.25f;

        GameObject.Destroy(quarter1);
        GameObject.Destroy(quarter2);

        _idleRoute[0] = middle;
        _idleRoute[1] = q1_1;
        _idleRoute[2] = _rightIdleTarget;
        _idleRoute[3] = q1_2;
        _idleRoute[4] = middle;
        _idleRoute[5] = q2_1;
        _idleRoute[6] = _leftIdleTarget;
        _idleRoute[7] = q2_2;
    }
}
