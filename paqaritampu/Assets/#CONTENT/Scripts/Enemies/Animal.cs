﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal : MonoBehaviour
{
    private AnimalManager _animalManager;
    private bool _animStart = false;
    private float _xOffset;

    private int _currentTargetID = 0;

    private float _lerpTime = 0.0f;

    // Update is called once per frame
    void Update()
    {
        if (_animStart)
        {
            transform.position = Vector3.Lerp(_animalManager.GetTarget(_currentTargetID).position, _animalManager.GetTarget(_currentTargetID + 1).position, _lerpTime);
            transform.localPosition = new Vector3(transform.localPosition.x + _xOffset, transform.localPosition.y, transform.localPosition.z);
            //transform.rotation = _animalManager.GetTarget(_currentTargetID).localRotation;
            transform.LookAt(_animalManager.GetTarget(_currentTargetID+1).position + new Vector3(_xOffset, 0.0f, 0.0f));

            _lerpTime += Time.deltaTime * _animalManager.GetSpeed();

            if (_lerpTime > 1.0f)
            {
                _lerpTime = 0.0f;

                if (_currentTargetID < _animalManager.GetPathLength() - 1)
                    _currentTargetID++;

                if (_currentTargetID == _animalManager.GetPathLength() - 1) //when last target reached -> destroy animals
                {
                    Destroy(gameObject);
                }
            }
        }
    }

    public void StartMovement()
    {
        int scatterfactor = _animalManager.GetScattering();
        _xOffset = _animalManager.GetRandom().Next(scatterfactor * -1, scatterfactor) / 100.0f;

        float timeOffset = _animalManager.GetRandom().Next(0, _animalManager.GetTimeOffset()) / 100.0f;

        StartCoroutine(StartWithOffset(timeOffset));
    }

    private IEnumerator StartWithOffset(float offset)
    {
        yield return new WaitForSeconds(offset);
        _animStart = true;
    }

    public void SetAnimalManager(AnimalManager manager)
    {
        _animalManager = manager;
    }
}
