﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WakeUpBat : MonoBehaviour
{
    [SerializeField] private BatController _batController;
    private OVRGrabbable _grabbable;

    private bool _grabbed = false;

    // Start is called before the first frame update
    void Start()
    {
        _grabbable = GetComponent<OVRGrabbable>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_grabbed && _grabbable.isGrabbed)
        {
            _grabbed = true;
            StartCoroutine(WaitForStart());
        }
    }

    private IEnumerator WaitForStart()
    {
        yield return new WaitForSeconds(2.5f);
        _batController.StartFlying();
    }
}
