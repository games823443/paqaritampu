﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatController : MonoBehaviour
{
    [SerializeField] private GameObject _target;
    [SerializeField] private float _speed = 4.0f;
    [SerializeField] private float _orbitSpeed = 50.0f;
    [SerializeField] private LayerMask _layerMask;

    [Header("Object Avoidance")]
    [SerializeField] private bool _useObjectAvoidance;
    [SerializeField] private float _lerpSpeed = 2.0f;
    [SerializeField] private float _raycastDistance = 0.3f;
    [SerializeField] private float _raycastLength = 1.0f;

    private float _health = 100.0f;

    private Animator _animator;
    private bool _started = false;

    private bool _orbit = false;
    private float _orbitSpeedAdjusted;
    private float _speedAdjusted;
    private bool _gotHit = false;

    private bool _drop = false;
    private Vector3 _dropPos;

    private bool _dead = false;

    private bool _attacking = false;
    private bool _reachedPlayer = false;
    private bool _cantAttack = false;
    private float _attackTimer = 0.0f;

    public bool _debugStart = false;

    

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _orbitSpeedAdjusted = _orbitSpeed;
        _speedAdjusted = _speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (_dead)
            return;

        if (_debugStart && !_started)
            StartFlying();

        if(_started && !_drop)
        {
            Vector3 _diff = _target.transform.position - transform.position;

            if ((!_reachedPlayer && Vector3.Magnitude(_diff) > 2.0f) || (_reachedPlayer && Vector3.Magnitude(_diff) > 2.5f))
            {
                if(_orbit)
                {
                    _orbit = false;
                    _animator.SetBool("orbit", false);

                    _attackTimer = 0.0f;
                }

                transform.position += Vector3.Normalize(_diff) * Time.deltaTime * _speed;
                transform.position = Vector3.Lerp(transform.position, transform.position + ObstacleAvoidance(Direction.FORWARD), Time.deltaTime * _lerpSpeed);
                transform.LookAt(_target.transform);
            }
            else
            {
                if (!_reachedPlayer)
                    _reachedPlayer = true;

                if (!_orbit)
                {
                    _orbit = true;
                    _animator.SetBool("orbit", true);
                }

                if(_attackTimer < 5.0f || _cantAttack)
                {
                    if (!_attacking)
                    {
                        if(Vector3.Magnitude(_diff) < 1.5f)
                        {
                            transform.position -= Vector3.Normalize(_diff) * Time.deltaTime * _speedAdjusted / 2.0f;
                            //transform.position = Vector3.Lerp(transform.position, transform.position + ObstacleAvoidance(Direction.BACKWARDS), Time.deltaTime * _lerpSpeed); 
                            transform.LookAt(_target.transform);
                        }

                        _attackTimer += Time.deltaTime;

                        if (_attackTimer >= 5.0f)
                            CanAttack();

                        transform.RotateAround(_target.transform.position, Vector3.up, _orbitSpeedAdjusted * Time.deltaTime);
                        transform.position = Vector3.Lerp(transform.position, transform.position + ObstacleAvoidance(Direction.LEFT), Time.deltaTime * _lerpSpeed);
                    }
                    else if(Vector3.Magnitude(_diff) > 0.3f)
                    {
                        transform.position += Vector3.Normalize(_diff) * Time.deltaTime * _speedAdjusted * Vector3.Magnitude(_diff);
                        transform.position = Vector3.Lerp(transform.position, transform.position + ObstacleAvoidance(Direction.FORWARD), Time.deltaTime * _lerpSpeed);
                        transform.LookAt(_target.transform);
                    }
                }
                else
                {
                    _animator.SetTrigger("attack");
                    _attackTimer = 0.0f;
                    _attacking = true;
                    StartCoroutine(WaitForEndOfAttack());
                }

            }
            transform.rotation = Quaternion.Euler(0.0f, transform.eulerAngles.y, 0.0f);
            transform.position = new Vector3(transform.position.x, _target.transform.position.y, transform.position.z);
        }
        else if(_drop)
        {
            if (transform.position.y <= _dropPos.y)
            {
                _animator.SetBool("hitGround", true);
                transform.position = new Vector3(transform.position.x, _dropPos.y + 0.02f, transform.position.z);
                transform.rotation = Quaternion.Euler(0.0f, transform.eulerAngles.y, transform.eulerAngles.z);
                _dead = true;
            }
            else
            {
                transform.position += Vector3.down * Time.deltaTime * (_speedAdjusted / 2.0f);
            }
        }
    }

    public void StartFlying()
    {
        if (_started)
            return;

        _animator.SetBool("start", true);
        _started = true;
    }

    private IEnumerator WaitForEndOfAttack()
    {
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);
        _attacking = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Axe") || collision.gameObject.CompareTag("Sickle"))
        {
            if (!_gotHit)
            {
                _gotHit = true;

                _health -= 20.0f;

                if (_health > 0.0f)
                {
                    _animator.SetTrigger("gotHit");
                    StartCoroutine(HitCooldown());
                }
                else
                {
                    _animator.SetBool("lastHit", true);
                    _drop = true;

                    RaycastHit _hit;

                    if (Physics.Raycast(transform.position, transform.up * -1.0f, out _hit, 10.0f))
                    {
                        _dropPos = _hit.point;
                    }
                    else
                    {
                        _dropPos = transform.position - transform.up * 10.0f;
                    }
                }
            }
        }
    }

    private IEnumerator HitCooldown()
    {
        yield return new WaitForSeconds(1.0f);
        _gotHit = false;
    }

    private Vector3 ObstacleAvoidance(Direction dir)
    {
        if (_useObjectAvoidance)
        {
            Vector3 direction = Vector3.zero;

            Vector3 returnValue = Vector3.zero;

            Vector3 leftRay = transform.position;
            Vector3 rightRay = transform.position;

            float distance = _raycastLength;

            if (dir == Direction.FORWARD)
            {
                direction = transform.forward;
                leftRay -= transform.right * _raycastDistance;
                rightRay += transform.right * _raycastDistance;
            }
            else if (dir == Direction.BACKWARDS)
            {
                direction = transform.forward * -1;
                leftRay -= transform.right * _raycastDistance;
                rightRay += transform.right * _raycastDistance;
            }
            else if (dir == Direction.RIGHT)
            {
                direction = transform.right;
                leftRay -= transform.forward * _raycastDistance;
                rightRay += transform.forward * _raycastDistance;
            }
            else if (dir == Direction.LEFT)
            {
                direction = transform.right * -1;
                leftRay -= transform.forward * _raycastDistance;
                rightRay += transform.forward * _raycastDistance;
            }

            RaycastHit hit;

            if (Physics.Raycast(transform.position, direction, out hit, _raycastLength, _layerMask))
            {
                returnValue += hit.normal;

                if (hit.distance < 2.0f)
                {
                    distance = hit.distance;
                }
            }

            if (Physics.Raycast(leftRay, direction, out hit, _raycastLength, _layerMask))
            {
                returnValue += hit.normal;

                if (hit.distance < 2.0f && hit.distance < distance)
                {
                    distance = hit.distance;
                }
            }

            if (Physics.Raycast(rightRay, direction, out hit, _raycastLength, _layerMask))
            {
                returnValue += hit.normal;

                if (hit.distance < 2.0f && hit.distance < distance)
                {
                    distance = hit.distance;
                }
            }

            if(distance < 2.0f)
            {
                if (distance/2 >= 0.25f)
                {
                    _speedAdjusted = _speed * distance/2;
                    _orbitSpeedAdjusted = _orbitSpeed * distance/2;
                }
                else
                {
                    _speedAdjusted = _speed * 0.25f;
                    _orbitSpeedAdjusted = _orbitSpeed * 0.25f;
                }
            }
            else
            {
                _speedAdjusted = _speed;
                _orbitSpeedAdjusted = _orbitSpeed;
            }

            Debug.DrawRay(transform.position, direction, Color.red);
            Debug.DrawRay(leftRay, direction, Color.green);
            Debug.DrawRay(rightRay, direction, Color.green);

            returnValue.y = 0.0f;

            return Vector3.Normalize(returnValue) * Time.deltaTime * 10.0f;
        }
        else
            return Vector3.zero;
    }

    private void CanAttack()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, Vector3.Magnitude(transform.position - _target.transform.position), _layerMask))
            _cantAttack = true;
        else
            _cantAttack = false;
    }
}

public enum Direction { FORWARD, BACKWARDS, RIGHT, LEFT}
