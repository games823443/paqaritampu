﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MMP3
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private GameObject _centerEye;

        [FMODUnity.EventRef]
        [SerializeField] private string _footstepsEvent;
        private FMOD.Studio.EventInstance _footstepsSound;

        private GameObject _sideways;

        private GameObject _returnCube;

        private void Start()
        {
            _footstepsSound = FMODUnity.RuntimeManager.CreateInstance(_footstepsEvent);
            _footstepsSound.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));

            _returnCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            _returnCube.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        }

        void Update()
        {
            Vector3 movement = _centerEye.transform.forward * Input.GetAxis("Vertical") * Time.deltaTime * 2.0f;
            movement.y = 0.0f;
            transform.position += movement;

            transform.localEulerAngles += new Vector3(0.0f, Input.GetAxis("Horizontal") * Time.deltaTime * 100.0f, 0.0f);

            if(_sideways != null)
            {
                if (Vector3.Angle(_centerEye.transform.forward, _sideways.transform.forward) > 45.0f)
                {
                    transform.position += _sideways.transform.forward*0.5f;
                    _sideways = null;
                }
            }
             
            if(movement.magnitude > 0.0f)
                PlayFootsteps();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Room")
                RoomManager.s_instance.EnterRoom(other.gameObject.GetComponent<Room>().ID);
            else if (other.tag == "Transition")
            {
                RoomTransition rt = other.gameObject.GetComponent<RoomTransition>();
                RoomManager.s_instance.SectionTransition(rt.Section1, rt.Section2);
            }
            else if(other.tag == "Sideways")
            {
                _sideways = other.gameObject;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Sideways")
                _sideways = null;
        }

        private void PlayFootsteps()
        {
            if (PlaybackState(_footstepsSound) != FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                int layer = 0;
                RaycastHit hit;
                if (Physics.Raycast(transform.position, new Vector3(0, -1, 0), out hit))
                {
                    layer = hit.collider.gameObject.layer;
                    layer -= 8;
                    if (layer < 0)
                        layer = 0;
                }

                _footstepsSound.setParameterByName("Surface", layer);
                _footstepsSound.setParameterByName("Player_Movement_States", 0);
                _footstepsSound.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
                _footstepsSound.start();
            }
        }

        private FMOD.Studio.PLAYBACK_STATE PlaybackState(FMOD.Studio.EventInstance instance)
        {
            FMOD.Studio.PLAYBACK_STATE pS;
            instance.getPlaybackState(out pS);
            return pS;
        }
    }
}
