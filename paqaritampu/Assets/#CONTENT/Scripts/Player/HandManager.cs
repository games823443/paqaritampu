﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandManager : MonoBehaviour
{
    [SerializeField] private GameObject _leftHand;
    [SerializeField] private GameObject _rightHand;
    [SerializeField] private GameObject _hint;
    [SerializeField] private Backpack _backpack;
    [SerializeField] private PhoneGrab _phone;

    [Header("Objects to Trigger")]
    [SerializeField] private DoorChildRoom _doorChildRoom;

    private OVRGrabber _leftGrabber;
    private OVRGrabber _rightGrabber;

    private bool _doorOpened = false;

    void Start()
    {
        _leftGrabber = _leftHand.GetComponent<OVRGrabber>();
        _rightGrabber = _rightHand.GetComponent<OVRGrabber>();
    }

    void Update()
    {
        if (_leftGrabber.grabbedObject != null)
            CheckGrabbedObjects(_leftGrabber);

        if (_rightGrabber.grabbedObject != null)
            CheckGrabbedObjects(_rightGrabber);

        if(Input.GetKeyDown(KeyCode.Z))
        {
            SoundEventOneShot[] soundEvents = _hint.GetComponents<SoundEventOneShot>();

            foreach (SoundEventOneShot s in soundEvents)
                s.PlaySound();

            if (!_doorOpened)
            {
                _doorChildRoom.OpenDoor();
                _doorOpened = true;
            }
        }
    }

    private void CheckGrabbedObjects(OVRGrabber hand)
    {
        if (hand.grabbedObject.tag == "Light")
            GameManager.s_instance.TakeDamage(Time.deltaTime);
        else if (hand.grabbedObject.tag == "Hint")
        {
            SoundEventOneShot[] soundEvents = hand.grabbedObject.GetComponents<SoundEventOneShot>();

            foreach(SoundEventOneShot s in soundEvents)
                s.PlaySound();

            if (!_doorOpened)
            {
                _doorChildRoom.OpenDoor();
                _doorOpened = true;
            }
        }
        else if (hand.grabbedObject.tag == "Backpack")
        {
            float distance = Vector3.Magnitude(hand.grabbedObject.transform.position - hand.transform.position);

            hand.ForceRelease(hand.grabbedObject);

            if (distance < 0.5f)
                _backpack.PickUpBackpack(hand.gameObject);

            // hand.grabbedObject.GetComponent<OVRGrabbable>().enabled = false;
        }
        else if (hand.grabbedObject.tag == "Phone")
        {
            float distance = Vector3.Magnitude(hand.grabbedObject.transform.position - hand.transform.position);

            hand.ForceRelease(hand.grabbedObject);

            if (distance < 0.1f)
                _phone.PickUpPhone(hand.gameObject);

            // hand.grabbedObject.GetComponent<OVRGrabbable>().enabled = false;
        }
    }

    public void DropEverything()
    {
        if (_leftGrabber.grabbedObject != null)
            _leftGrabber.ForceRelease(_leftGrabber.grabbedObject);

        if (_rightGrabber.grabbedObject != null)
            _rightGrabber.ForceRelease(_rightGrabber.grabbedObject);
    }
}
