﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerTrigger : MonoBehaviour
{
    [SerializeField] private GameObject _finger;

    void Update()
    {
        transform.position = _finger.transform.position;
        transform.rotation = _finger.transform.rotation;
    }
}
